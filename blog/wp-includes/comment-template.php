<?php
/**
 * Comment template functions
 *
 * These functions are meant to live inside of the WordPress loop.
 *
 * @package WordPress
 * @subpackage Template
 */

/**
 * Retrieve the author of the current comment.
 *
 * If the comment has an empty comment_author field, then 'Anonymous' person is
 * assumed.
 *
 * @since 1.5.0
 * @since 4.4.0 Added the ability for `$comment_ID` to also accept a WP_Comment object.
 *
 * @param int|WP_Comment $comment_ID Optional. WP_Comment or the ID of the comment for which to retrieve the author.
 *									 Default current comment.
 * @return string The comment author
 */
function get_comment_author( $comment_ID = 0 ) {
	$comment = get_comment( $comment_ID );

	if ( empty( $comment->comment_author ) ) {
		if ( $comment->user_id && $user = get_userdata( $comment->user_id ) )
			$author = $user->display_name;
		else
			$author = __('Anonymous');
	} else {
		$author = $comment->comment_author;
	}

	/**
	 * Filters the returned comment author name.
	 *
	 * @since 1.5.0
	 * @since 4.1.0 The `$comment_ID` and `$comment` parameters were added.
	 *
	 * @param string     $author     The comment author's username.
	 * @param int        $comment_ID The comment ID.
	 * @param WP_Comment $comment    The comment object.
	 */
	return apply_filters( 'get_comment_author', $author, $comment->comment_ID, $comment );
}

/**
 * Displays the author of the current comment.
 *
 * @since 0.71
 * @since 4.4.0 Added the ability for `$comment_ID` to also accept a WP_Comment object.
 *
 * @param int|WP_Comment $comment_ID Optional. WP_Comment or the ID of the comment for which to print the author.
 *									 Default current comment.
 */
function comment_author( $comment_ID = 0 ) {
	$comment = get_comment( $comment_ID );
	$author  = get_comment_author( $comment );

	/**
	 * Filters the comment author's name for display.
	 *
	 * @since 1.2.0
	 * @since 4.1.0 The `$comment_ID` parameter was added.
	 *
	 * @param string $author     The comment author's username.
	 * @param int    $comment_ID The comment ID.
	 */
	echo apply_filters( 'comment_author', $author, $comment->comment_ID );
}

/**
 * Retrieve the email of the author of the current comment.
 *
 * @since 1.5.0
 * @since 4.4.0 Added the ability for `$comment_ID` to also accept a WP_Comment object.
 *
 * @param int|WP_Comment $comment_ID Optional. WP_Comment or the ID of the comment for which to get the author's email.
 *									 Default current comment.
 * @return string The current comment author's email
 */
function get_comment_author_email( $comment_ID = 0 ) {
	$comment = get_comment( $comment_ID );

	/**
	 * Filters the comment author's returned email address.
	 *
	 * @since 1.5.0
	 * @since 4.1.0 The `$comment_ID` and `$comment` parameters were added.
	 *
	 * @param string     $comment_author_email The comment author's email address.
	 * @param int        $comment_ID           The comment ID.
	 * @param WP_Comment $comment              The comment object.
	 */
	return apply_filters( 'get_comment_author_email', $comment->comment_author_email, $comment->comment_ID, $comment );
}

/**
 * Display the email of the author of the current global $comment.
 *
 * Care should be taken to protect the email address and assure that email
 * harvesters do not capture your commentors' email address. Most assume that
 * their email address will not appear in raw form on the site. Doing so will
 * enable anyone, including those that people don't want to get the email
 * address and use it for their own means good and bad.
 *
 * @since 0.71
 * @since 4.4.0 Added the ability for `$comment_ID` to also accept a WP_Comment object.
 *
 * @param int|WP_Comment $comment_ID Optional. WP_Comment or the ID of the comment for which to print the author's email.
 *									 Default current comment.
 */
function comment_author_email( $comment_ID = 0 ) {
	$comment      = get_comment( $comment_ID );
	$author_email = get_comment_author_email( $comment );

	/**
	 * Filters the comment author's email for display.
	 *
	 * @since 1.2.0
	 * @since 4.1.0 The `$comment_ID` parameter was added.
	 *
	 * @param string $author_email The comment author's email address.
	 * @param int    $comment_ID   The comment ID.
	 */
	echo apply_filters( 'author_email', $author_email, $comment->comment_ID );
}

/**
 * Display the html email link to the author of the current comment.
 *
 * Care should be taken to protect the email address and assure that email
 * harvesters do not capture your commentors' email address. Most assume that
 * their email address will not appear in raw form on the site. Doing so will
 * enable anyone, including those that people don't want to get the email
 * address and use it for their own means good and bad.
 *
 * @since 0.71
 * @since 4.6.0 Added the `$comment` parameter.
 *
 * @param string         $linktext Optional. Text to display instead of the comment author's email address.
 *                                 Default empty.
 * @param string         $before   Optional. Text or HTML to display before the email link. Default empty.
 * @param string         $after    Optional. Text or HTML to display after the email link. Default empty.
 * @param int|WP_Comment $comment  Optional. Comment ID or WP_Comment object. Default is the current comment.
 */
function comment_author_email_link( $linktext = '', $before = '', $after = '', $comment = null ) {
	if ( $link = get_comment_author_email_link( $linktext, $before, $after, $comment ) ) {
		echo $link;
	}
}

/**
 * Return the html email link to the author of the current comment.
 *
 * Care should be taken to protect the email address and assure that email
 * harvesters do not capture your commentors' email address. Most assume that
 * their email address will not appear in raw form on the site. Doing so will
 * enable anyone, including those that people don't want to get the email
 * address and use it for their own means good and bad.
 *
 * @since 2.7.0
 * @since 4.6.0 Added the `$comment` parameter.
 *
 * @param string         $linktext Optional. Text to display instead of the comment author's email address.
 *                                 Default empty.
 * @param string         $before   Optional. Text or HTML to display before the email link. Default empty.
 * @param string         $after    Optional. Text or HTML to display after the email link. Default empty.
 * @param int|WP_Comment $comment  Optional. Comment ID or WP_Comment object. Default is the current comment.
 * @return string HTML markup for the comment author email link. By default, the email address is obfuscated
 *                via the {@see 'comment_email'} filter with antispambot().
 */
function get_comment_author_email_link( $linktext = '', $before = '', $after = '', $comment = null ) {
	$comment = get_comment( $comment );

	/**
	 * Filters the comment author's email for display.
	 *
	 * Care should be taken to protect the email address and assure that email
	 * harvesters do not capture your commenter's email address.
	 *
	 * @since 1.2.0
	 * @since 4.1.0 The `$comment` parameter was added.
	 *
	 * @param string     $comment_author_email The comment author's email address.
	 * @param WP_Comment $comment              The comment object.
	 */
	$email = apply_filters( 'comment_email', $comment->comment_author_email, $comment );

	if ((!empty($email)) && ($email != '@')) {
	$display = ($linktext != '') ? $linktext : $email;
		$return  = $before;
		$return .= sprintf( '<a href="%1$s">%2$s</a>', esc_url( 'mailto:' . $email ), esc_html( $display ) );
	 	$return .= $after;
		return $return;
	} else {
		return '';
	}
}

/**
 * Retrieve the HTML link to the URL of the author of the current comment.
 *
 * Both get_comment_author_url() and get_comment_author() rely on get_comment(),
 * which falls back to the global comment variable if the $comment_ID argument is empty.
 *
 * @since 1.5.0
 * @since 4.4.0 Added the ability for `$comment_ID` to also accept a WP_Comment object.
 *
 * @param int|WP_Comment $comment_ID Optional. WP_Comment or the ID of the comment for which to get the author's link.
 *									 Default current comment.
 * @return string The comment author name or HTML link for author's URL.
 */
function get_comment_author_link( $comment_ID = 0 ) {
	$comment = get_comment( $comment_ID );
	$url     = get_comment_author_url( $comment );
	$author  = get_comment_author( $comment );

	if ( empty( $url ) || 'http://' == $url )
		$return = $author;
	else
		$return = "<a href='$url' rel='external nofollow' class='url'>$author</a>";

	/**
	 * Filters the comment author's link for display.
	 *
	 * @since 1.5.0
	 * @since 4.1.0 The `$author` and `$comment_ID` parameters were added.
	 *
	 * @param string $return     The HTML-formatted comment author link.
	 *                           Empty for an invalid URL.
	 * @param string $author     The comment author's username.
	 * @param int    $comment_ID The comment ID.
	 */
	return apply_filters( 'get_comment_author_link', $return, $author, $comment->comment_ID );
}

/**
 * Display the html link to the url of the author of the current comment.
 *
 * @since 0.71
 * @since 4.4.0 Added the ability for `$comment_ID` to also accept a WP_Comment object.
 *
 * @param int|WP_Comment $comment_ID Optional. WP_Comment or the ID of the comment for which to print the author's link.
 *									 Default current comment.
 */
function comment_author_link( $comment_ID = 0 ) {
	echo get_comment_author_link( $comment_ID );
}

/**
 * Retrieve the IP address of the author of the current comment.
 *
 * @since 1.5.0
 * @since 4.4.0 Added the ability for `$comment_ID` to also accept a WP_Comment object.
 *
 * @param int|WP_Comment $comment_ID Optional. WP_Comment or the ID of the comment for which to get the author's IP address.
 *									 Default current comment.
 * @return string Comment author's IP address.
 */
function get_comment_author_IP( $comment_ID = 0 ) {
	$comment = get_comment( $comment_ID );

	/**
	 * Filters the comment author's returned IP address.
	 *
	 * @since 1.5.0
	 * @since 4.1.0 The `$comment_ID` and `$comment` parameters were added.
	 *
	 * @param string     $comment_author_IP The comment author's IP address.
	 * @param int        $comment_ID        The comment ID.
	 * @param WP_Comment $comment           The comment object.
	 */
	return apply_filters( 'get_comment_author_IP', $comment->comment_author_IP, $comment->comment_ID, $comment );
}

/**
 * Display the IP address of the author of the current comment.
 *
 * @since 0.71
 * @since 4.4.0 Added the ability for `$comment_ID` to also accept a WP_Comment object.
 *
 * @param int|WP_Comment $comment_ID Optional. WP_Comment or the ID of the comment for which to print the author's IP address.
 *									 Default current comment.
 */
function comment_author_IP( $comment_ID = 0 ) {
	echo esc_html( get_comment_author_IP( $comment_ID ) );
}

/**
 * Retrieve the url of the author of the current comment.
 *
 * @since 1.5.0
 * @since 4.4.0 Added the ability for `$comment_ID` to also accept a WP_Comment object.
 *
 * @param int|WP_Comment $comment_ID Optional. WP_Comment or the ID of the comment for which to get the author's URL.
 *									 Default current comment.
 * @return string Comment author URL.
 */
function get_comment_author_url( $comment_ID = 0 ) {
	$comment = get_comment( $comment_ID );
	$url = '';
	$id = 0;
	if ( ! empty( $comment ) ) {
		$author_url = ( 'http://' == $comment->comment_author_url ) ? '' : $comment->comment_author_url;
		$url = esc_url( $author_url, array( 'http', 'https' ) );
		$id = $comment->comment_ID;
	}

	/**
	 * Filters the comment author's URL.
	 *
	 * @since 1.5.0
	 * @since 4.1.0 The `$comment_ID` and `$comment` parameters were added.
	 *
	 * @param string     $url        The comment author's URL.
	 * @param int        $comment_ID The comment ID.
	 * @param WP_Comment $comment    The comment object.
	 */
	return apply_filters( 'get_comment_author_url', $url, $id, $comment );
}

/**
 * Display the url of the author of the current comment.
 *
 * @since 0.71
 * @since 4.4.0 Added the ability for `$comment_ID` to also accept a WP_Comment object.
 *
 * @param int|WP_Comment $comment_ID Optional. WP_Comment or the ID of the comment for which to print the author's URL.
 *									 Default current comment.
 */
function comment_author_url( $comment_ID = 0 ) {
	$comment    = get_comment( $comment_ID );
	$author_url = get_comment_author_url( $comment );

	/**
	 * Filters the comment author's URL for display.
	 *
	 * @since 1.2.0
	 * @since 4.1.0 The `$comment_ID` parameter was added.
	 *
	 * @param string $author_url The comment author's URL.
	 * @param int    $comment_ID The comment ID.
	 */
	echo apply_filters( 'comment_url', $author_url, $comment->comment_ID );
}

/**
 * Retrieves the HTML link of the url of the author of the current comment.
 *
 * $linktext parameter is only used if the URL does not exist for the comment
 * author. If the URL does exist then the URL will be used and the $linktext
 * will be ignored.
 *
 * Encapsulate the HTML link between the $before and $after. So it will appear
 * in the order of $before, link, and finally $after.
 *
 * @since 1.5.0
 * @since 4.6.0 Added the `$comment` parameter.
 *
 * @param string         $linktext Optional. The text to display instead of the comment
 *                                 author's email address. Default empty.
 * @param string         $before   Optional. The text or HTML to display before the email link.
 *                                 Default empty.
 * @param string         $after    Optional. The text or HTML to display after the email link.
 *                                 Default empty.
 * @param int|WP_Comment $comment  Optional. Comment ID or WP_Comment object.
 *                                 Default is the current comment.
 * @return string The HTML link between the $before and $after parameters.
 */
function get_comment_author_url_link( $linktext = '', $before = '', $after = '', $comment = 0 ) {
	$url = get_comment_author_url( $comment );
	$display = ($linktext != '') ? $linktext : $url;
	$display = str_replace( 'http://www.', '', $display );
	$display = str_replace( 'http://', '', $display );

	if ( '/' == substr($display, -1) ) {
		$display = substr($display, 0, -1);
	}

	$return = "$before<a href='$url' rel='external'>$display</a>$after";

	/**
	 * Filters the comment author's returned URL link.
	 *
	 * @since 1.5.0
	 *
	 * @param string $return The HTML-formatted comment author URL link.
	 */
	return apply_filters( 'get_comment_author_url_link', $return );
}

/**
 * Displays the HTML link of the url of the author of the current comment.
 *
 * @since 0.71
 * @since 4.6.0 Added the `$comment` parameter.
 *
 * @param string         $linktext Optional. Text to display instead of the comment author's
 *                                 email address. Default empty.
 * @param string         $before   Optional. Text or HTML to display before the email link.
 *                                 Default empty.
 * @param string         $after    Optional. Text or HTML to display after the email link.
 *                                 Default empty.
 * @param int|WP_Comment $comment  Optional. Comment ID or WP_Comment object.
 *                                 Default is the current comment.
 */
function comment_author_url_link( $linktext = '', $before = '', $after = '', $comment = 0 ) {
	echo get_comment_author_url_link( $linktext, $before, $after, $comment );
}

/**
 * Generates semantic classes for each comment element.
 *
 * @since 2.7.0
 * @since 4.4.0 Added the ability for `$comment` to also accept a WP_Comment object.
 *
 * @param string|array   $class    Optional. One or more classes to add to the class list.
 *                                 Default empty.
 * @param int|WP_Comment $comment  Comment ID or WP_Comment object. Default current comment.
 * @param int|WP_Post    $post_id  Post ID or WP_Post object. Default current post.
 * @param bool           $echo     Optional. Whether to cho or return the output.
 *                                 Default true.
 * @return string If `$echo` is false, the class will be returned. Void otherwise.
 */
function comment_class( $class = '', $comment = null, $post_id = null, $echo = true ) {
	// Separates classes with a single space, collates classes for comment DIV
	$class = 'class="' . join( ' ', get_comment_class( $class, $comment, $post_id ) ) . '"';
	if ( $echo)
		echo $class;
	else
		return $class;
}

/**
 * Returns the classes for the comment div as an array.
 *
 * @since 2.7.0
 * @since 4.4.0 Added the ability for `$comment_id` to also accept a WP_Comment object.
 *
 * @global int $comment_alt
 * @global int $comment_depth
 * @global int $comment_thread_alt
 *
 * @param string|array   $class      Optional. One or more classes to add to the class list. Default empty.
 * @param int|WP_Comment $comment_id Comment ID or WP_Comment object. Default current comment.
 * @param int|WP_Post    $post_id    Post ID or WP_Post object. Default current post.
 * @return array An array of classes.
 */
function get_comment_class( $class = '', $comment_id = null, $post_id = null ) {
	global $comment_alt, $comment_depth, $comment_thread_alt;

	$classes = array();

	$comment = get_comment( $comment_id );
	if ( ! $comment ) {
		return $classes;
	}

	// Get the comment type (comment, trackback),
	$classes[] = ( empty( $comment->comment_type ) ) ? 'comment' : $comment->comment_type;

	// Add classes for comment authors that are registered users.
	if ( $comment->user_id > 0 && $user = get_userdata( $comment->user_id ) ) {
		$classes[] = 'byuser';
		$classes[] = 'comment-author-' . sanitize_html_class( $user->user_nicename, $comment->user_id );
		// For comment authors who are the author of the post
		if ( $post = get_post($post_id) ) {
			if ( $comment->user_id === $post->post_author ) {
				$classes[] = 'bypostauthor';
			}
		}
	}

	if ( empty($comment_alt) )
		$comment_alt = 0;
	if ( empty($comment_depth) )
		$comment_depth = 1;
	if ( empty($comment_thread_alt) )
		$comment_thread_alt = 0;

	if ( $comment_alt % 2 ) {
		$classes[] = 'odd';
		$classes[] = 'alt';
	} else {
		$classes[] = 'even';
	}

	$comment_alt++;

	// Alt for top-level comments
	if ( 1 == $comment_depth ) {
		if ( $comment_thread_alt % 2 ) {
			$classes[] = 'thread-odd';
			$classes[] = 'thread-alt';
		} else {
			$classes[] = 'thread-even';
		}
		$comment_thread_alt++;
	}

	$classes[] = "depth-$comment_depth";

	if ( !empty($class) ) {
		if ( !is_array( $class ) )
			$class = preg_split('#\s+#', $class);
		$classes = array_merge($classes, $class);
	}

	$classes = array_map('esc_attr', $classes);

	/**
	 * Filters the returned CSS classes for the current comment.
	 *
	 * @since 2.7.0
	 *
	 * @param array       $classes    An array of comment classes.
	 * @param string      $class      A comma-separated list of additional classes added to the list.
	 * @param int         $comment_id The comment id.
	 * @param WP_Comment  $comment    The comment object.
	 * @param int|WP_Post $post_id    The post ID or WP_Post object.
	 */
	return apply_filters( 'comment_class', $classes, $class, $comment->comment_ID, $comment, $post_id );
}

/**
 * Retrieve the comment date of the current comment.
 *
 * @since 1.5.0
 * @since 4.4.0 Added the ability for `$comment_ID` to also accept a WP_Comment object.
 *
 * @param string          $d          Optional. The format of the date. Default user's setting.
 * @param int|WP_Comment  $comment_ID WP_Comment or ID of the comment for which to get the date.
 *                                    Default current comment.
 * @return string The comment's date.
 */
function get_comment_date( $d = '', $comment_ID = 0 ) {
	$comment = get_comment( $comment_ID );
	if ( '' == $d )
		$date = mysql2date(get_option('date_format'), $comment->comment_date);
	else
		$date = mysql2date($d, $comment->comment_date);
	/**
	 * Filters the returned comment date.
	 *
	 * @since 1.5.0
	 *
	 * @param string|int $date    Formatted date string or Unix timestamp.
	 * @param string     $d       The format of the date.
	 * @param WP_Comment $comment The comment object.
	 */
	return apply_filters( 'get_comment_date', $date, $d, $comment );
}

/**
 * Display the comment date of the current comment.
 *
 * @since 0.71
 * @since 4.4.0 Added the ability for `$comment_ID` to also accept a WP_Comment object.
 *
 * @param string         $d          Optional. The format of the date. Default user's settings.
 * @param int|WP_Comment $comment_ID WP_Comment or ID of the comment for which to print the date.
 *                                   Default current comment.
 */
function comment_date( $d = '', $comment_ID = 0 ) {
	echo get_comment_date( $d, $comment_ID );
}

/**
 * Retrieve the excerpt of the current comment.
 *
 * Will cut each word and only output the first 20 words with '&hellip;' at the end.
 * If the word count is less than 20, then no truncating is done and no '&hellip;'
 * will appear.
 *
 * @since 1.5.0
 * @since 4.4.0 Added the ability for `$comment_ID` to also accept a WP_Comment object.
 *
 * @param int|WP_Comment $comment_ID  WP_Comment or ID of the comment for which to get the excerpt.
 *                                    Default current comment.
 * @return string The maybe truncated comment with 20 words or less.
 */
function get_comment_excerpt( $comment_ID = 0 ) {
	$comment = get_comment( $comment_ID );
	$comment_text = strip_tags( str_replace( array( "\n", "\r" ), ' ', $comment->comment_content ) );
	$words = explode( ' ', $comment_text );

	/**
	 * Filters the amount of words used in the comment excerpt.
	 *
	 * @since 4.4.0
	 *
	 * @param int $comment_excerpt_length The amount of words you want to display in the comment excerpt.
	 */
	$comment_excerpt_length = apply_filters( 'comment_excerpt_length', 20 );

	$use_ellipsis = count( $words ) > $comment_excerpt_length;
	if ( $use_ellipsis ) {
		$words = array_slice( $words, 0, $comment_excerpt_length );
	}

	$excerpt = trim( join( ' ', $words ) );
	if ( $use_ellipsis ) {
		$excerpt .= '&hellip;';
	}
	/**
	 * Filters the retrieved comment excerpt.
	 *
	 * @since 1.5.0
	 * @since 4.1.0 The `$comment_ID` and `$comment` parameters were added.
	 *
	 * @param string     $excerpt    The comment excerpt text.
	 * @param int        $comment_ID The comment ID.
	 * @param WP_Comment $comment    The comment object.
	 */
	return apply_filters( 'get_comment_excerpt', $excerpt, $comment->comment_ID, $comment );
}

/**
 * Display the excerpt of the current comment.
 *
 * @since 1.2.0
 * @since 4.4.0 Added the ability for `$comment_ID` to also accept a WP_Comment object.
 *
 * @param int|WP_Comment $comment_ID  WP_Comment or ID of the comment for which to print the excerpt.
 *                                    Default current comment.
 */
function comment_excerpt( $comment_ID = 0 ) {
	$comment         = get_comment( $comment_ID );
	$comment_excerpt = get_comment_excerpt( $comment );

	/**
	 * Filters the comment excerpt for display.
	 *
	 * @since 1.2.0
	 * @since 4.1.0 The `$comment_ID` parameter was added.
	 *
	 * @param string $comment_excerpt The comment excerpt text.
	 * @param int    $comment_ID      The comment ID.
	 */
	echo apply_filters( 'comment_excerpt', $comment_excerpt, $comment->comment_ID );
}

/**
 * Retrieve the comment id of the current comment.
 *
 * @since 1.5.0
 *
 * @return int The comment ID.
 */
function get_comment_ID() {
	$comment = get_comment();

	/**
	 * Filters the returned comment ID.
	 *
	 * @since 1.5.0
	 * @since 4.1.0 The `$comment_ID` parameter was added.
	 *
	 * @param int        $comment_ID The current comment ID.
	 * @param WP_Comment $comment    The comment object.
	 */
	return apply_filters( 'get_comment_ID', $comment->comment_ID, $comment );
}

/**
 * Display the comment id of the current comment.
 *
 * @since 0.71
 */
function comment_ID() {
	echo get_comment_ID();
}

/**
 * Retrieve the link to a given comment.
 *
 * @since 1.5.0
 * @since 4.4.0 Added the ability for `$comment` to also accept a WP_Comment object. Added `$cpage` argument.
 *
 * @see get_page_of_comment()
 *
 * @global WP_Rewrite $wp_rewrite
 * @global bool       $in_comment_loop
 *
 * @param WP_Comment|int|null $comment Comment to retrieve. Default current comment.
 * @param array               $args {
 *     An array of optional arguments to override the defaults.
 *
 *     @type string     $type      Passed to get_page_of_comment().
 *     @type int        $page      Current page of comments, for calculating comment pagination.
 *     @type int        $per_page  Per-page value for comment pagination.
 *     @type int        $max_depth Passed to get_page_of_comment().
 *     @type int|string $cpage     Value to use for the comment's "comment-page" or "cpage" value.
 *                                 If provided, this value overrides any value calculated from `$page`
 *                                 and `$per_page`.
 * }
 * @return string The permalink to the given comment.
 */
function get_comment_link( $comment = null, $args = array() ) {
	global $wp_rewrite, $in_comment_loop;

	$comment = get_comment($comment);

	// Back-compat.
	if ( ! is_array( $args ) ) {
		$args = array( 'page' => $args );
	}

	$defaults = array(
		'type'      => 'all',
		'page'      => '',
		'per_page'  => '',
		'max_depth' => '',
		'cpage'     => null,
	);
	$args = wp_parse_args( $args, $defaults );

	$link = get_permalink( $comment->comment_post_ID );

	// The 'cpage' param takes precedence.
	if ( ! is_null( $args['cpage'] ) ) {
		$cpage = $args['cpage'];

	// No 'cpage' is provided, so we calculate one.
	} else {
		if ( '' === $args['per_page'] && get_option( 'page_comments' ) ) {
			$args['per_page'] = get_option('comments_per_page');
		}

		if ( empty( $args['per_page'] ) ) {
			$args['per_page'] = 0;
			$args['page'] = 0;
		}

		$cpage = $args['page'];

		if ( '' == $cpage ) {
			if ( ! empty( $in_comment_loop ) ) {
				$cpage = get_query_var( 'cpage' );
			} else {
				// Requires a database hit, so we only do it when we can't figure out from context.
				$cpage = get_page_of_comment( $comment->comment_ID, $args );
			}
		}

		/*
		 * If the default page displays the oldest comments, the permalinks for comments on the default page
		 * do not need a 'cpage' query var.
		 */
		if ( 'oldest' === get_option( 'default_comments_page' ) && 1 === $cpage ) {
			$cpage = '';
		}
	}

	if ( $cpage && get_option( 'page_comments' ) ) {
		if ( $wp_rewrite->using_permalinks() ) {
			if ( $cpage ) {
				$link = trailingslashit( $link ) . $wp_rewrite->comments_pagination_base . '-' . $cpage;
			}

			$link = user_trailingslashit( $link, 'comment' );
		} elseif ( $cpage ) {
			$link = add_query_arg( 'cpage', $cpage, $link );
		}

	}

	if ( $wp_rewrite->using_permalinks() ) {
		$link = user_trailingslashit( $link, 'comment' );
	}

	$link = $link . '#comment-' . $comment->comment_ID;

	/**
	 * Filters the returned single comment permalink.
	 *
	 * @since 2.8.0
	 * @since 4.4.0 Added the `$cpage` parameter.
	 *
	 * @see get_page_of_comment()
	 *
	 * @param string     $link    The comment permalink with '#comment-$id' appended.
	 * @param WP_Comment $comment The current comment object.
	 * @param array      $args    An array of arguments to override the defaults.
	 * @param int        $cpage   The calculated 'cpage' value.
	 */
	return apply_filters( 'get_comment_link', $link, $comment, $args, $cpage );
}

/**
 * Retrieves the link to the current post comments.
 *
 * @since 1.5.0
 *
 * @param int|WP_Post $post_id Optional. Post ID or WP_Post object. Default is global $post.
 * @return string The link to the comments.
 */
function get_comments_link( $post_id = 0 ) {
	$hash = get_comments_number( $post_id ) ? '#comments' : '#respond';
	$comments_link = get_permalink( $post_id ) . $hash;

	/**
	 * Filters the returned post comments permalink.
	 *
	 * @since 3.6.0
	 *
	 * @param string      $comments_link Post comments permalink with '#comments' appended.
	 * @param int|WP_Post $post_id       Post ID or WP_Post object.
	 */
	return apply_filters( 'get_comments_link', $comments_link, $post_id );
}

/**
 * Display the link to the current post comments.
 *
 * @since 0.71
 *
 * @param string $deprecated   Not Used.
 * @param string $deprecated_2 Not Used.
 */
function comments_link( $deprecated = '', $deprecated_2 = '' ) {
	if ( !empty( $deprecated ) )
		_deprecated_argument( __FUNCTION__, '0.72' );
	if ( !empty( $deprecated_2 ) )
		_deprecated_argument( __FUNCTION__, '1.3.0' );
	echo esc_url( get_comments_link() );
}

/**
 * Retrieves the amount of comments a post has.
 *
 * @since 1.5.0
 *
 * @param int|WP_Post $post_id Optional. Post ID or WP_Post object. Default is the global `$post`.
 * @return string|int If the post exists, a numeric string representing the number of comments
 *                    the post has, otherwise 0.
 */
function get_comments_number( $post_id = 0 ) {
	$post = get_post( $post_id );

	if ( ! $post ) {
		$count = 0;
	} else {
		$count = $post->comment_count;
		$post_id = $post->ID;
	}

	/**
	 * Filters the returned comment count for a post.
	 *
	 * @since 1.5.0
	 *
	 * @param string|int $count   A string representing the number of comments a post has, otherwise 0.
	 * @param int        $post_id Post ID.
	 */
	return apply_filters( 'get_comments_number', $count, $post_id );
}

/**
 * Display the language string for the number of comments the current post has.
 *
 * @since 0.71
 *
 * @param string $zero       Optional. Text for no comments. Default false.
 * @param string $one        Optional. Text for one comment. Default false.
 * @param string $more       Optional. Text for more than one comment. Default false.
 * @param string $deprecated Not used.
 */
function comments_number( $zero = false, $one = false, $more = false, $deprecated = '' ) {
	if ( ! empty( $deprecated ) ) {
		_deprecated_argument( __FUNCTION__, '1.3.0' );
	}
	echo get_comments_number_text( $zero, $one, $more );
}

/**
 * Display the language string for the number of comments the current post has.
 *
 * @since 4.0.0
 *
 * @param string $zero Optional. Text for no comments. Default false.
 * @param string $one  Optional. Text for one comment. Default false.
 * @param string $more Optional. Text for more than one comment. Default false.
 */
function get_comments_number_text( $zero = false, $one = false, $more = false ) {
	$number = get_comments_number();

	if ( $number > 1 ) {
		if ( false === $more ) {
			/* translators: %s: number of comments */
			$output = sprintf( _n( '%s Comment', '%s Comments', $number ), number_format_i18n( $number ) );
		} else {
			// % Comments
			/* translators: If comment number in your language requires declension,
			 * translate this to 'on'. Do not translate into your own language.
			 */
			if ( 'on' === _x( 'off', 'Comment number declension: on or off' ) ) {
				$text = preg_replace( '#<span class="screen-reader-text">.+?</span>#', '', $more );
				$text = preg_replace( '/&.+?;/', '', $text ); // Kill entities
				$text = trim( strip_tags( $text ), '% ' );

				// Replace '% Comments' with a proper plural form
				if ( $text && ! preg_match( '/[0-9]+/', $text ) && false !== strpos( $more, '%' ) ) {
					/* translators: %s: number of comments */
					$new_text = _n( '%s Comment', '%s Comments', $number );
					$new_text = trim( sprintf( $new_text, '' ) );

					$more = str_replace( $text, $new_text, $more );
					if ( false === strpos( $more, '%' ) ) {
						$more = '% ' . $more;
					}
				}
			}

			$output = str_replace( '%', number_format_i18n( $number ), $more );
		}
	} elseif ( $number == 0 ) {
		$output = ( false === $zero ) ? __( 'No Comments' ) : $zero;
	} else { // must be one
		$output = ( false === $one ) ? __( '1 Comment' ) : $one;
	}
	/**
	 * Filters the comments count for display.
	 *
	 * @since 1.5.0
	 *
	 * @see _n()
	 *
	 * @param string $output A translatable string formatted based on whether the count
	 *                       is equal to 0, 1, or 1+.
	 * @param int    $number The number of post comments.
	 */
	return apply_filters( 'comments_number', $output, $number );
}

/**
 * Retrieve the text of the current comment.
 *
 * @since 1.5.0
 * @since 4.4.0 Added the ability for `$comment_ID` to also accept a WP_Comment object.
 *
 * @see Walker_Comment::comment()
 *
 * @param int|WP_Comment  $comment_ID WP_Comment or ID of the comment for which to get the text.
 *                                    Default current comment.
 * @param array           $args       Optional. An array of arguments. Default empty.
 * @return string The comment content.
 */
function get_comment_text( $comment_ID = 0, $args = array() ) {
	$comment = get_comment( $comment_ID );

	/**
	 * Filters the text of a comment.
	 *
	 * @since 1.5.0
	 *
	 * @see Walker_Comment::comment()
	 *
	 * @param string     $comment_content Text of the comment.
	 * @param WP_Comment $comment         The comment object.
	 * @param array      $args            An array of arguments.
	 */
	return apply_filters( 'get_comment_text', $comment->comment_content, $comment, $args );
}

/**
 * Display the text of the current comment.
 *
 * @since 0.71
 * @since 4.4.0 Added the ability for `$comment_ID` to also accept a WP_Comment object.
 *
 * @see Walker_Comment::comment()
 *
 * @param int|WP_Comment  $comment_ID WP_Comment or ID of the comment for which to print the text.
 *                                    Default current comment.
 * @param array           $args       Optional. An array of arguments. Default empty array. Default empty.
 */
function comment_text( $comment_ID = 0, $args = array() ) {
	$comment = get_comment( $comment_ID );

	$comment_text = get_comment_text( $comment, $args );
	/**
	 * Filters the text of a comment to be displayed.
	 *
	 * @since 1.2.0
	 *
	 * @see Walker_Comment::comment()
	 *
	 * @param string          $comment_text Text of the current comment.
	 * @param WP_Comment|null $comment      The comment object.
	 * @param array           $args         An array of arguments.
	 */
	echo apply_filters( 'comment_text', $comment_text, $comment, $args );
}

/**
 * Retrieve the comment time of the current comment.
 *
 * @since 1.5.0
 *
 * @param string $d         Optional. The format of the time. Default user's settings.
 * @param bool   $gmt       Optional. Whether to use the GMT date. Default false.
 * @param bool   $translate Optional. Whether to translate the time (for use in feeds).
 *                          Default true.
 * @return string The formatted time.
 */
function get_comment_time( $d = '', $gmt = false, $translate = true ) {
	$comment = get_comment();

	$comment_date = $gmt ? $comment->comment_date_gmt : $comment->comment_date;
	if ( '' == $d )
		$date = mysql2date(get_option('time_format'), $comment_date, $translate);
	else
		$date = mysql2date($d, $comment_date, $translate);

	/**
	 * Filters the returned comment time.
	 *
	 * @since 1.5.0
	 *
	 * @param string|int $date      The comment time, formatted as a date string or Unix timestamp.
	 * @param string     $d         Date format.
	 * @param bool       $gmt       Whether the GMT date is in use.
	 * @param bool       $translate Whether the time is translated.
	 * @param WP_Comment $comment   The comment object.
	 */
	return apply_filters( 'get_comment_time', $date, $d, $gmt, $translate, $comment );
}

/**
 * Display the comment time of the current comment.
 *
 * @since 0.71
 *
 * @param string $d Optional. The format of the time. Default user's settings.
 */
function comment_time( $d = '' ) {
	echo get_comment_time($d);
}

/**
 * Retrieve the comment type of the current comment.
 *
 * @since 1.5.0
 * @since 4.4.0 Added the ability for `$comment_ID` to also accept a WP_Comment object.
 *
 * @param int|WP_Comment $comment_ID Optional. WP_Comment or ID of the comment for which to get the type.
 *                                   Default current comment.
 * @return string The comment type.
 */
function get_comment_type( $comment_ID = 0 ) {
	$comment = get_comment( $comment_ID );
	if ( '' == $comment->comment_type )
		$comment->comment_type = 'comment';

	/**
	 * Filters the returne